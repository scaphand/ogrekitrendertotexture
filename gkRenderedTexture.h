#ifndef GKRENDEREDTEXTURE_H
#define GKRENDEREDTEXTURE_H

#include <OgreKit.h>

#include <OgreHardwarePixelBuffer.h>
#include <OgreRenderTargetListener.h>

class gkRenderedTexture : public Ogre::RenderTargetListener
{
    public:

        gkRenderedTexture(gkString textureName, gkString materialName);
        virtual ~gkRenderedTexture();

        void setCamera(gkCamera *referenceCamera);
        void setReflexion(gkVector3 reflexionPlanePoint1, gkVector3 reflexionPlanePoint2, gkVector3 reflexionPlanePoint3);
        void setProjectiveTexturing(bool isProjective);

    protected:
        bool isProjective;
        gkCamera* referenceCamera;

        Ogre::RenderTexture *renderTexture;
        Ogre::TextureUnitState* textureUnitState;

        virtual void preRenderTargetUpdate(const Ogre::RenderTargetEvent& evt);
        virtual void postRenderTargetUpdate(const Ogre::RenderTargetEvent& evt);
    private:
};

#endif // GKRENDEREDTEXTURE_H
