# CMAKE generated file: DO NOT EDIT!
# Generated by "MinGW Makefiles" Generator, CMake Version 2.8

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:

# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list

# Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

SHELL = cmd.exe

# The CMake executable.
CMAKE_COMMAND = "C:\Program Files (x86)\CMake 2.8\bin\cmake.exe"

# The command to remove a file.
RM = "C:\Program Files (x86)\CMake 2.8\bin\cmake.exe" -E remove -f

# Escaping for special characters.
EQUALS = =

# The program to use to edit the cache.
CMAKE_EDIT_COMMAND = "C:\Program Files (x86)\CMake 2.8\bin\cmake-gui.exe"

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = C:\vr\OgreKitRenderToTexture

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = C:\vr\OgreKitRenderToTexture\build

# Include any dependencies generated for this target.
include CMakeFiles/OgreKitRenderToTexture.dir/depend.make

# Include the progress variables for this target.
include CMakeFiles/OgreKitRenderToTexture.dir/progress.make

# Include the compile flags for this target's objects.
include CMakeFiles/OgreKitRenderToTexture.dir/flags.make

CMakeFiles/OgreKitRenderToTexture.dir/main.cpp.obj: CMakeFiles/OgreKitRenderToTexture.dir/flags.make
CMakeFiles/OgreKitRenderToTexture.dir/main.cpp.obj: CMakeFiles/OgreKitRenderToTexture.dir/includes_CXX.rsp
CMakeFiles/OgreKitRenderToTexture.dir/main.cpp.obj: ../main.cpp
	$(CMAKE_COMMAND) -E cmake_progress_report C:\vr\OgreKitRenderToTexture\build\CMakeFiles $(CMAKE_PROGRESS_1)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object CMakeFiles/OgreKitRenderToTexture.dir/main.cpp.obj"
	c:\MinGW\bin\g++.exe   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles\OgreKitRenderToTexture.dir\main.cpp.obj -c C:\vr\OgreKitRenderToTexture\main.cpp

CMakeFiles/OgreKitRenderToTexture.dir/main.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/OgreKitRenderToTexture.dir/main.cpp.i"
	c:\MinGW\bin\g++.exe  $(CXX_DEFINES) $(CXX_FLAGS) -E C:\vr\OgreKitRenderToTexture\main.cpp > CMakeFiles\OgreKitRenderToTexture.dir\main.cpp.i

CMakeFiles/OgreKitRenderToTexture.dir/main.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/OgreKitRenderToTexture.dir/main.cpp.s"
	c:\MinGW\bin\g++.exe  $(CXX_DEFINES) $(CXX_FLAGS) -S C:\vr\OgreKitRenderToTexture\main.cpp -o CMakeFiles\OgreKitRenderToTexture.dir\main.cpp.s

CMakeFiles/OgreKitRenderToTexture.dir/main.cpp.obj.requires:
.PHONY : CMakeFiles/OgreKitRenderToTexture.dir/main.cpp.obj.requires

CMakeFiles/OgreKitRenderToTexture.dir/main.cpp.obj.provides: CMakeFiles/OgreKitRenderToTexture.dir/main.cpp.obj.requires
	$(MAKE) -f CMakeFiles\OgreKitRenderToTexture.dir\build.make CMakeFiles/OgreKitRenderToTexture.dir/main.cpp.obj.provides.build
.PHONY : CMakeFiles/OgreKitRenderToTexture.dir/main.cpp.obj.provides

CMakeFiles/OgreKitRenderToTexture.dir/main.cpp.obj.provides.build: CMakeFiles/OgreKitRenderToTexture.dir/main.cpp.obj

# Object files for target OgreKitRenderToTexture
OgreKitRenderToTexture_OBJECTS = \
"CMakeFiles/OgreKitRenderToTexture.dir/main.cpp.obj"

# External object files for target OgreKitRenderToTexture
OgreKitRenderToTexture_EXTERNAL_OBJECTS =

OgreKitRenderToTexture.exe: CMakeFiles/OgreKitRenderToTexture.dir/main.cpp.obj
OgreKitRenderToTexture.exe: CMakeFiles/OgreKitRenderToTexture.dir/build.make
OgreKitRenderToTexture.exe: libgkRenderedTexture.a
OgreKitRenderToTexture.exe: ../../glew/lib/libglew32.a
OgreKitRenderToTexture.exe: CMakeFiles/OgreKitRenderToTexture.dir/objects1.rsp
OgreKitRenderToTexture.exe: CMakeFiles/OgreKitRenderToTexture.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --red --bold "Linking CXX executable OgreKitRenderToTexture.exe"
	$(CMAKE_COMMAND) -E cmake_link_script CMakeFiles\OgreKitRenderToTexture.dir\link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
CMakeFiles/OgreKitRenderToTexture.dir/build: OgreKitRenderToTexture.exe
.PHONY : CMakeFiles/OgreKitRenderToTexture.dir/build

CMakeFiles/OgreKitRenderToTexture.dir/requires: CMakeFiles/OgreKitRenderToTexture.dir/main.cpp.obj.requires
.PHONY : CMakeFiles/OgreKitRenderToTexture.dir/requires

CMakeFiles/OgreKitRenderToTexture.dir/clean:
	$(CMAKE_COMMAND) -P CMakeFiles\OgreKitRenderToTexture.dir\cmake_clean.cmake
.PHONY : CMakeFiles/OgreKitRenderToTexture.dir/clean

CMakeFiles/OgreKitRenderToTexture.dir/depend:
	$(CMAKE_COMMAND) -E cmake_depends "MinGW Makefiles" C:\vr\OgreKitRenderToTexture C:\vr\OgreKitRenderToTexture C:\vr\OgreKitRenderToTexture\build C:\vr\OgreKitRenderToTexture\build C:\vr\OgreKitRenderToTexture\build\CMakeFiles\OgreKitRenderToTexture.dir\DependInfo.cmake --color=$(COLOR)
.PHONY : CMakeFiles/OgreKitRenderToTexture.dir/depend

