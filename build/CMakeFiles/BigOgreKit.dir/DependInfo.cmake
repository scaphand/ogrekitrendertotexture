# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  )
# The set of files for implicit dependencies of each language:

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "../../gamekit/Engine"
  "../../gamekit/Engine/AI"
  "../../gamekit/build-mingw/Engine"
  "../../gamekit/Ogre-1.9a/OgreMain/include"
  "../../gamekit/build-mingw/include"
  "../../gamekit/Dependencies/Source/GameKit/Utils"
  "../../gamekit/bullet/src"
  "../../gamekit/Dependencies/Source/GameKit/AnimKit"
  "../../gamekit/Dependencies/Source/OIS/include"
  "../../gamekit/Dependencies/Source/Lua/lua"
  "../../gamekit/Dependencies/Source/OpenAL"
  "../../gamekit/Ogre-1.9a/Components/Overlay/include"
  "../../gamekit/Ogre-1.9a/RenderSystems/GL/include"
  "../../gamekit/Dependencies/Source/tclap/include"
  "../../glew/include"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
