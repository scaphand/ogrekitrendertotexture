#include "gkRenderedTexture.h"

#include <OgreRoot.h>

gkRenderedTexture::gkRenderedTexture(gkString textureName, gkString materialName)
{
    /// Create render texture material
    Ogre::TexturePtr rtt_texture = Ogre::TextureManager::getSingleton().createManual(textureName, Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, Ogre::TEX_TYPE_2D,
                                                                                     1920, 1080,
                                                                                     0, Ogre::PF_R8G8B8, Ogre::TU_RENDERTARGET);

    /// Create render texture
    renderTexture = rtt_texture->getBuffer()->getRenderTarget();

    Ogre::MaterialPtr renderMaterial = Ogre::MaterialManager::getSingleton().create(materialName, Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
    Ogre::Technique* matTechnique = renderMaterial->createTechnique();
    matTechnique->createPass();
    renderMaterial->getTechnique(0)->getPass(0)->setLightingEnabled(false);
    textureUnitState = renderMaterial->getTechnique(0)->getPass(0)->createTextureUnitState(textureName);

    renderTexture->setAutoUpdated(true);
    renderTexture->addListener(this);
}

gkRenderedTexture::~gkRenderedTexture()
{
}

void gkRenderedTexture::setCamera(gkCamera *referenceCamera)
{
    this->referenceCamera = referenceCamera;
    renderTexture->addViewport(referenceCamera->getCamera());
    renderTexture->getViewport(0)->setClearEveryFrame(true);
    //renderTexture->getViewport(0)->setBackgroundColour(Ogre::ColourValue::Red);
    renderTexture->getViewport(0)->setOverlaysEnabled(false);
}

void gkRenderedTexture::setReflexion(gkVector3 reflexionPlanePoint1, gkVector3 reflexionPlanePoint2, gkVector3 reflexionPlanePoint3)
{
    if (referenceCamera)
    {
        Ogre::MovablePlane *plane = new Ogre::MovablePlane(reflexionPlanePoint1,reflexionPlanePoint2,reflexionPlanePoint3);

        referenceCamera->getCamera()->enableReflection(plane);
        referenceCamera->getCamera()->enableCustomNearClipPlane(plane);
    }
}

void gkRenderedTexture::setProjectiveTexturing(bool isProjective)
{
    if (referenceCamera && textureUnitState)
    {
        textureUnitState->setProjectiveTexturing(isProjective, referenceCamera->getCamera());
    }
}

void gkRenderedTexture::preRenderTargetUpdate(const Ogre::RenderTargetEvent& evt)
{
    //m_engine->getActiveScene()->getObject("Plane")->setVisible(false);
}

void gkRenderedTexture::postRenderTargetUpdate(const Ogre::RenderTargetEvent& evt)
{
    //m_engine->getActiveScene()->getObject("Plane")->setVisible(true);
}
